#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : .
## USAGE : 
## AUTHORS : KooK.dev@free.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

from Appli import BaseAppli

VERSION = "0.1"
PP = pprint.PrettyPrinter(indent=4)

# ========================================
class myAppli( BaseAppli ):

	def _mainWork( self ):
		print( "La fonction principale" )

		self.verbose( "En mode verbeux." )
		return
	# _mainWork

	def __defineOptions( self ):
		# positionnal / mandatory param
		#	type = int,?
		self._o_parser.add_argument( 'param1', type=int,
						   help='a parameter.' )
		self._o_parser.add_argument( 'infile',
								type=argparse.FileType( 'r' ),
								default=sys.stdin,
								help='The infile.' )

		# optionnal
		self._o_parser.add_argument( '-f', dest='opt1',
								default='toto',
								help='a flag with a value.' )
		self._o_parser.add_argument( '--outfile', '-o',
								type=argparse.FileType( 'w' ),
								default=sys.stdout,
								help='outfile, default is stdout.' )

		# mandatory option
		#	destination is opt2 by default
		self._o_parser.add_argument( '--opt2', '-o',
							action='store_true', # store the value as boolean
							help='a flag opt, long and short.')


# ========================================
# ----------------------------------------

if __name__ == '__main__':
	descr = 'Prog descr.'
	o_app = myAppli( "template", VERSION, descr=descr )
	o_app.run()

	sys.exit( 0 )

