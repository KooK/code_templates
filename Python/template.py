#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : .
## USAGE : 
## AUTHORS : KooK.dev@free.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

VERSION = "0.1"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
def main( o_args ):
#def main( infile, outfile ):
	print( "La fonction principale" )

	verbose( "En mode verbeux." )

	return 0

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int,?
	o_parser.add_argument( 'param1', type=int,
		               help='a parameter.' )
	o_parser.add_argument( 'infile',
							type=argparse.FileType( 'r' ),
							default=sys.stdin,
							help='The infile.' )

	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )
	o_parser.add_argument( '-f', dest='opt1',
							default='toto',
							help='a flag with a value.' )
	o_parser.add_argument( '--outfile', '-o',
							type=argparse.FileType( 'w' ),
							default=sys.stdout,
							help='outfile, default is stdout.' )

	# mandatory option
	#	destination is opt2 by default
	o_parser.add_argument( '--opt2', '-o',
						action='store_true', # store the value as boolean
						help='a flag opt, long and short.')

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True


def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
#	PP.pprint( o_args )

#	infile  = sys.argv[1]
#	outfile = sys.argv[2]
#	main( infile, outfile )
	main( o_args )

	sys.exit( 0 )

