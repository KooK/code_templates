#! /usr/bin/perl

## AIMS  : .
## USAGE : x.pl [-h] [-v] --infile toto

# Copyright 2014 Sébastien Letort
# Copyright 2016 Sébastien Letort
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;

# Standard
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
use Term::ANSIColor;
use FindBin qw( $RealBin );

# not standard
# use Exception::Class ( "OptionException" );

# path to your libraries
use lib "/your path/lib";

use constant DEBUG => 1;
my $VERBOSE = 0;

# ==================================================
MAIN:
{
	my $rh_options = &defineOptions();
	&debug( $rh_options );

	my $rh_params  = &analyseParameters_Options( $rh_options );
	&debug( { options => $rh_options, params => $rh_params } );

	exit( 0 );
}


##  =====================================
sub verbose
{
	my( $o_fs, $ra_msg ) = @_;

	if( 1 == $VERBOSE )
	{
		map{ print $o_fs $_ } @$ra_msg;
	}

	return;
}

sub analyseParameters_Options
{
	my( $rh_options ) = @_;

	my @a_keys = qw( infile );
	my( $infile )
	           = @$rh_options{ @a_keys };

	if( !defined $infile )
	{
		my $msg = "infile must be provided.\n";
		OptionException->throw( error => $msg );
	}
	my %h_params      = ();
#	$h_params{ infile } = new IO::File( $infile, 'r' );

	if( $$rh_options{ verbose } )
	{
		$VERBOSE = 1;

		warn "\n╔═══════════════════════════════════════════════════ ═ ═ ═ ═ ═ ═\n";
		warn "║─ SCRIPT  : $0\n";
		warn "║─ INFILE  : $infile\n";
		warn "╚═══════════════════════════════════════════════════ ═ ═ ═ ═ ═ ═\n\n";
	}

	return \%h_params;
} # analyseOptions

sub defineOptions
{
	Getopt::Long::Configure( qw( auto_help auto_version ) );

	my @a_options  = qw( infile=s verbose );
	my $rh_options = { infile => '-' };
	GetOptions( $rh_options, @a_options )
		or pod2usage();

	#mandatory
	my @a_mand_keys = qw( infile );
	&__assertMandatory( $rh_options, \@a_mand_keys );

	return $rh_options;

	sub __assertMandatory
	{
		my( $rh_options, $ra_keys ) = @_;

		foreach my $key ( @$ra_keys )
		{
			if( !defined $$rh_options{ $key } )
			{
				&__usage();

				my $msg = "\n\t*$key* is a mandatory attribute, you have to set it.\n\n";
				die( $msg );
			}
		}
	}

	sub __usage
	{

		my $msg = "at least one argument is missing.\n";
		warn colored( [ 'bold blue on_white' ], $msg );

		my $ra_colors = ['bold red'];
		$msg  = "try '" . colored( $ra_colors, "--help" );
		$msg .= "', '" . colored( $ra_colors, "-h" );
		$msg .= "', '" . colored( $ra_colors, "-?" );
		$msg .= "' options to see how to use the program\n";
		warn $msg;

		$msg  = "You can also use '" . colored( $ra_colors, "perldoc $0" );
		$msg .= "' to have complete documentation\n";
		warn $msg;

		return;
	}

} # fin de defineOptions

sub debug
{
	return if( 1 != &DEBUG );

	warn Dumper @_;

	return;
}


__END__

=pod

=encoding utf8

=head1 NAME

 x.pl - do things.

=head1 SYNOPSIS

 x.pl --infile toto [--verbose]
 x.pl --help/-h

=head1 DESCRIPTION

 This program will do things

=head1 LICENCE

 GPLv3

=head1 AUTHOR

 Sébastien Letort - seb.letort_job@laposte.net
 KooK - KooK.dev@free.fr

=cut
