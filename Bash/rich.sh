#! /bin/bash

## AIMS  : 
## USAGE : ./x.sh [-h] [-v] -i stg
## ALGO  : 
#
## OUTPUT :
## OUTFILE:
#
## NOTE  :
#
## BUG   : None found yet.

set -o nounset
set -o errexit

# =========================================================
declare INFILE;
declare VAR;

# ---------------------------------------------------------
# declare -r MG_MAP_DIR="/PUBLIC_DATA/GENERATED/1kG/map_files"
declare VERBOSE=0

trap before_exiting EXIT

# =========================================================
function before_exiting ()
{
	printf "TRAP : $0 exit with code $?.\n" >&2
}

function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INFILE     = $INFILE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =============================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "i:hv" opt
	do
		case $opt in
			i)	VAR=$OPTARG  ;;

			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	INFILE="${1?Please name the infile}"
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] infile

Options :
	-i string : any variable

	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

# =========================================================
function main ()
{
	printf "Fonction principale.\n"
}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
