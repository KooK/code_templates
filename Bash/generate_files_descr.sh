#! /bin/sh

## AIMS  : generate files.desc file, a file that briefly describe programs found in "directory".
##		It is based on AIMS and USAGE comment.
## USAGE : generate_files_descr.sh directory
#
## WARN : It will erase previous file

readonly DIR=${1?'Which directory ?'}

readonly OUTFILE=$DIR/files.descr

header ()
{
	# header
	printf "## $DIR file descriptions\n"
	printf "## generated on %s\n" $( date +%Y/%m/%d )
	printf "#\n"
	printf "## FILE GENERATED AUTOMATICALLY *DO NOT EDIT*\n"
	printf "#\n\n"
}

extractLine ()
{
	local key="$1"
	local file="$2"

	grep "^## $key" $file \
		| perl -pe 's/^## /\t/'
}

doTheJob ()
{
	local flag=0

	for filepath in $DIR/*
	do
		file=$( basename $filepath )

		# tester avec file ?
		ext=${file##*.}

		# is there a better way ? I'm quite sure there is !
		if [ "$ext" = "sh" -o "$ext" = "pl" \
				-o "$ext" = "py" -o "$ext" = "php" ]
		then
			printf "$file\n"
			extractLine 'AIMS' $filepath
			extractLine 'USAGE' $filepath
			flag=$(( flag+1 ))
		fi
	done

	return $flag
}

# -------------------

header > $OUTFILE
doTheJob >> $OUTFILE

# echo "$?"

if [ 0 -eq $? ]
then
	# no file found to describe
	rm -f $OUTFILE
fi

exit 0
