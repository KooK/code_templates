#! /bin/bash

## AIMS  : 
## USAGE :
## NOTE  :
## AUTHORS : KooK.dev@free.fr

set -o nounset
set -o errexit

# =========================================================
declare -r INFILE="${1?What is your input file ?}"

# ---------------------------------------------------------
#declare -r MG_MAP_DIR="/PUBLIC_DATA/GENERATED/1kG/map_files"

trap before_exiting EXIT

# =========================================================
function before_exiting ()
{
	printf "TRAP : $0 exit with code $?.\n" >&2
}

function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INFILE     = $INFILE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function main ()
{
	printf "Fonction principale.\n"
}

# =========================================================
printVars >&2

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2

exit 0
