#include <iostream>
#include <exception>
#include <vector>
#include <string>
#include <cstdlib> // rand
#include <ctime>   // time

#include "Appli.hpp"

using namespace std;

class NumberGenerator
{
	private :
		int __min, __max, __range;

	public :
		NumberGenerator( int min, int max )
			: __min( min ), __max( max ), __range( max-min )
		{ srand( (unsigned) time(0) ); }

		int generate()
		{
			return ( __min + int( rand() % __range ) );
		}

		// nb = nb random integer
		std::vector<int> generate( short nb )
		{
			std::vector<int> v_out;

			for( int i=0; i<nb; ++i )
			{
				int n = this->generate();
				v_out.push_back( n );
			}

			return v_out;
		}

};

class myBaseAppli : public BaseAppli
{
	short __nb, __nb_repeat;
	int __min, __max;
	std::string __end_msg;

	public:
		myBaseAppli( const string& name, const string& version )
			: BaseAppli( name, version )
		{// std::cerr << "myBaseAppli1 ... " << endl;
			__defineOptions();
		}
		myBaseAppli( const string& name )
			: BaseAppli( name )
		{// std::cerr << "myBaseAppli2 ... " << endl;
			__defineOptions();
//			_cmdline_options.add( __config );
//			_config_file_options.add( __config );
		}

		void __defineOptions()
		{
			std::cout << "We define options for this (fake) application, a number generator :" <<endl;
			std::cout << "\t'nb' will define how many numbers are generated ;" <<endl;
			std::cout << "\t'repeat' or 'R' will specify how many time numbers will be generated ;" <<endl;
			std::cout << "\t'slow' or 'S', an option to tell to wait between generation." <<endl;

			po::options_description __options( "options" );
			__options.add_options()
				( "nb", po::value<short>( &__nb )->default_value(5), "defines how many numbers are generated." )
				( "repeat,R", po::value<short>()->implicit_value(2), "re-run the generation x times." )
				( "slow,S", "go slow, wait for keystroke between generation." )
			;

			std::cout << "We also define some arguments :" << endl;
			std::cout << "\t'min' is the lower limit of the generation ;" << std::endl;
			std::cout << "\t'max' is the upper limit of the generation." << std::endl;
			std::cout << "\t'msg' or 'M', a required argument. The message to display after the generation." <<endl;

			po::options_description __descr_arguments( "arguments" );
			boost::shared_ptr<po::option_description> __arg1 (
				new po::option_description( "min", po::value<int>( &__min )->default_value(0), "mini" ) );
			boost::shared_ptr<po::option_description> __arg2 (
				new po::option_description( "max", po::value<int>( &__max )->default_value(100), "maxi" ) );
			__descr_arguments.add( __arg1 );
			__descr_arguments.add( __arg2 );

			// an required option
			boost::shared_ptr<po::option_description> __arg3 (
				new po::option_description( "msg", po::value<std::string>( &__end_msg )->required(), "end message" ) );
			__descr_arguments.add( __arg3 );

			_cmdline_options.add( __options )
							.add( __descr_arguments );


			_positional.add( "min", 1 );
			_positional.add( "max", 1 );
		}

	protected :
		void _mainWork()
		{
			cerr
			 << "min = " << __min
				<< "\nmax = " << __max
				<< "\nnb = " << __nb
				<< endl;

			__nb_repeat = 1;
			if( _vm.count( "repeat" ) )
				__nb_repeat = _vm["repeat"].as<short>();
				cerr << "repeat = " << __nb_repeat << endl;

			if( _vm.count( "slow" ) )
				cerr << "we will go slow.\n";


			NumberGenerator ng( __min, __max );
			for( int i=0; i<__nb_repeat; ++i )
			{
				std::vector<int> v_num = ng.generate( __nb );

				cerr << "generated : ";
				for( std::vector<int>::const_iterator i=v_num.begin();
						i != v_num.end();++i )
					cerr << *i << " ";
				cerr << endl;
			}

			cout << __end_msg << endl;
		}

/* 	void setOptions()
	{

		// Hidden options, will be allowed both on command line and
		// in config file, but will not be shown to the user.
		po::options_description hidden("Hidden options");
		hidden.add_options()
			("input-file", po::value< vector<string> >(), "input file")
			;

		_config_file_options.add(config).add(hidden);
		_cmdline_options.add( config ).add( hidden );
//		_visible( "Allowed options" );
		_visible.add( config );
	}
*/
};

/*
	void __setConfigOptions()
	{
		__config.add_options()
			( "optimization,O", po::value<int>(&__opt)->default_value(10), 
				  "optimization level" )
			// "composing" pour dire que ce qui est dans le fichier de conf sera ajouté à ceux de la ligne de commande
			( "include-path,I", 
				 po::value< vector<string> >()->composing(), 
				 "include path" )
			;
	}

*/

// ===================================

void showAppli( const BaseAppli &app )
{
	cout << "Showing Appli : "
			<< app.getName() << " - " << app.getVersion()
			<< endl;
	return;
}


int main( int argc, char** argv )
{
	try
	{
		cout << "This program test the Appli Class.\n" << endl;
		cout << "OK ! -start-" << endl;

		cout << "----\nWe create a myBaseAppli, une spécialisation de Appli" << endl;
		myBaseAppli app1( "NumberGenerator" );
	//	showAppli( app1 );
		app1.run( argc, argv );
cerr << "\n\n";


/*		myBaseAppli app2( "Best App", "1.0" );
		showAppli( app2 );
		app2.run( argc, argv );
*/	}
	catch( exception &e )
	{
		cerr << "erreur : " << e.what()
			<< endl;
	}
	catch( ... )
	{
		cerr << "Program dies on an unknown exception."
			<< endl;
	}

	return 0;
}
