#ifndef __APPLI_HPP__
#define __APPLI_HPP__

#include <string>
#include "boost/program_options.hpp"

namespace po = boost::program_options;

// compilation (je ne comrpends pas le .a)
// g++ -std=c++0x *.cpp /usr/lib/libboost_program_options.a

// tmp
#include <iostream>

// only command line.
class BaseAppli
{

public :
	BaseAppli( const std::string &name="NoName", const std::string &version="0.1.0" )
		: __name( name ), __version( version ),
			__generic( "basic options" )
	{ //std::cerr << "cstr " << __name << " et " << __version << std::endl;
		__addGenericOptions();
	}

	~BaseAppli()
	{// std::cerr << "\n**destroy " << __name << " et " << __version << std::endl;
	}

	const std::string& getName() const
	{	return __name;	}
	const std::string& getVersion() const
	{	return __version;	}

	void run( int argc, char** argv )
	{
		// traitement des options

		// first source is the retained one.
		//if an option is found on cmd line, this option in cfg is ignored.
//		store( parse_command_line( argc, argv, _cmdline_options ), _vm );
		po::options_description cmdline;
		cmdline.add( _cmdline_options );
		cmdline.add( _cmdline_arguments );

//		po::positional_options_description positional;
//		positional.add( _cmdline_arguments );

		po::store( po::command_line_parser( argc, argv )
						.options( cmdline )
						.positional( _positional )
						.run(),
					_vm );

		if( false == __treatGenericOptions() )
			return;

		// notify after treatment of generic option
		//  to manage -h when required option defined
		po::notify( _vm );

		_mainWork();

		return;
	}

	// return false to exit, true otherwise
	bool __treatGenericOptions()
	{
		if( _vm.count( "help" ) )
		{
			std::cout << getName() << " - " << getVersion() << std::endl;
			std::cout << _cmdline_options << "\n";
			return false;
		}
		if( _vm.count( "version" ) )
		{
			std::cout << getName() << " - " << getVersion() << std::endl;
			return false;
		}

		return true;
	}

private :
	const std::string __name;
	const std::string __version;
	po::options_description __generic;

	void __addGenericOptions()
	{
		// allowed only on command line
		__generic.add_options()
			( "help,h", "produce help message and exit.")
			( "version,v", "print name & version and exit.")
			;
		_cmdline_options.add( __generic );
	}

protected :
	po::variables_map _vm;
	po::options_description _cmdline_options;
	po::options_description _cmdline_arguments;
	po::positional_options_description _positional;

	virtual void _mainWork() = 0;
//	{ std::cerr << "baseAppli:mainWork"; }

};

#endif /* __APPLI_HPP__ */
//			( "cfg", "filepath of the configuration file" )
//	po::options_description _visible;

//	po::options_description _config;
//	po::options_description _hidden;

//	void _set?CmdLineOptions();
/*	void setOptions()
	{
std::cerr << "Appli.setOptions() -> debut\n";
		// Hidden options, will be allowed both on command line and
		// in config file, but will not be shown to the user.
		po::options_description hidden("Hidden options");
		hidden.add_options()
			("input-file", po::value< vector<string> >(), "input file")
			;

		// combinaison de ces options dans une seule variable
		_cmdline_options.add( __generic ); //.add(config).add(hidden);

//		_config_file_options.add(config).add(hidden);

//		_visible( "Allowed options" );
//		_visible.add( __generic ); //.add(config);
std::cerr << "Appli.setOptions() -> fin\n";
	}
*/
//		store( parse_config_file( "example.cfg", desc), vm );
// 	po::options_description _config_file_options;
