#include <string>
#include "boost/program_options.hpp"

namespace po = boost::program_options;
using namespace std;

// compilation (je ne comrpends pas le .a)
// g++ -std=c++0x *.cpp /usr/lib/libboost_program_options.a

// tmp
#include <iostream>


void run( int argc, char** argv )
{
// -----------------
	po::options_description __generic( "generic options" );
	__generic.add_options()
		( "help,h", "produce help message and exit.")
		( "version,v", "print name & version and exit.")
		;

// -----------------
	po::options_description __options( "options" );
	__options.add_options()
		( "nb", po::value<short>()->default_value(5), "defines how many number are generated." )
		( "repeat,R", po::value<short>()->implicit_value(2), "re-run the generation x times." )
		( "slow,S", "go slow, wait for keystroke between generation." )
	;

// -----------------
	// mettre une valeur par défaut à un argument est absurde !
	// c'est la définition d'une option.
	po::options_description __arguments( "arguments" );
	boost::shared_ptr<po::option_description> __arg1 (
		new po::option_description( "min", po::value<int>()->default_value(0), "mini" ) );
	boost::shared_ptr<po::option_description> __arg2 (
		new po::option_description( "max", po::value<int>()->default_value(1), "maxi" ) );
	__arguments.add( __arg1 );
	__arguments.add( __arg2 );
//	__arguments.add_options()
//		( "min", po::value<int>()->default_value(0), "mini" )
//		( "max", po::value<int>()->default_value(1), "maxi" )
//	;

// -----------------
	po::options_description _cmdline_options;
	_cmdline_options.add( __generic )
					.add( __options );

// -----------------
	po::options_description _cmdline;
	_cmdline.add( _cmdline_options )
			.add( __arguments );

// -----------------
// ---- add argument ?
	po::positional_options_description _positional;
	_positional.add( "min", 1 );
	_positional.add( "max", 1 );

// -----------------
	po::variables_map _vm;
	po::store( po::command_line_parser( argc, argv )
					.options( _cmdline )
					.positional( _positional ).run(),
				_vm );

// -----------------
	if( _vm.count( "help" ) )
	{
		std::cout << "nom - version" << std::endl;
		std::cout << "Usage :\n"
					<< "\tnom [generic option]\n"
					<< "\tnom [options] arguments"
					<< endl;
		std::cout << _cmdline_options << "\n";
/* // voir option_description::canonical_display_name(); pas en v0.46
//		option_description::long_name(); // sans --
//		option_description::description();
//		option_description::format_name(); // avec --
//		option_description::format_parameter(); // ce qui suit --opt dans l'aide
// pour un argument il faut donc long_name sur la ligne "usage"
//		et ne pas avoir --format_name sur la ligne de description
//	=> besion d'accéder à la méthode options_description::operator<<
*/

		// format_name ajoute '--' au nom de l'argument
		std::cout << "arg1 : " << __arg1->format_name() << ", " << __arg1->description()
				<< "\narg2 : " << __arg2->long_name() << ", " << __arg2->description()
			<< endl;
		std::cout << __arguments << "\n";
		return;
	}
	if( _vm.count( "version" ) )
	{
		std::cout << "nom - version" << std::endl;
		return;
	}

	if( _vm.count( "slow" ) )
		std::cout << "Now we go slow !\n\b";
	if( _vm.count( "repeat" ) )
		std::cout << "We will repeat this " << _vm["repeat"].as<short>() << " times.\n";

	if( _vm.count( "nb" ) )
		std::cout << "We will use " << _vm["nb"].as<short>() << std::endl;

	if( _vm.count( "min" ) )
		std::cout << "min = " << _vm["min"].as<int>() << std::endl;
	if( _vm.count( "max" ) )
		std::cout << "max = " << _vm["max"].as<int>() << std::endl;

	return;
}


int main( int argc, char** argv )
{
	try
	{
		cout << "OK ! -start-" << endl;
		run( argc, argv );
		cerr << "\n\n";
	}
	catch( exception &e )
	{
		cerr << "erreur : " << e.what()
			<< endl;
	}
	catch( ... )
	{
		cerr << "Program dies on an unknown exception."
			<< endl;
	}

	return 0;
}
